package com.thinh;

import java.awt.event.KeyEvent;
import java.lang.reflect.Method;

import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import com.thinh.utils.Settings;
import com.thinh.utils.ShortcutsTyper;
import com.thinh.web.selenium.WebHelper;

public class AbstractTest {

    protected String browser;
    protected String username;
    protected String password;
    protected String ticket;
    protected String url;
    
    // fields
    private static final String[] results = { "PASSED", "FAILED", "UNKNOWN" };

    
    public AbstractTest() {
        browser = Settings.getParameter("browser");
        username = Settings.getParameter("username");
        password = Settings.getParameter("password");
        url = Settings.getParameter("JiraURL");
        ticket = Settings.getParameter("ticket");
    }
    
    // before suite
    @BeforeSuite(alwaysRun = true)
    public void beforeSuite() {
        
    }
    
    

    // after suite
    @AfterSuite(alwaysRun = true)
    public void afterSuite() {
        
    }

    // before test
    @BeforeTest(alwaysRun = true)
    public void beforeTest(ITestContext ctx) {
        // note: a <session> in our concept is a <suite> to TestNG
        // a <suite> in our concept is a <test> to TestNG

        String suiteName = ctx.getCurrentXmlTest().getName();
        if (suiteName == null)
            suiteName = "Automation";
        
       setup();

    }
    
    public void setup() {
        WebHelper helper = WebHelper.getHelper(browser);
        helper.goTo(url);
        // now bring the browser to foreground
        ShortcutsTyper.cmdShiftMaskPress(KeyEvent.VK_TAB);
    }

    // after test
    @AfterTest(alwaysRun = true)
    public void afterTest() {
        cleanTest();
    }
    
    public void cleanTest() {
        WebHelper.getHelper().quit();
        ShortcutsTyper.delayTime(1000);
    }

    // before method
    @BeforeMethod(alwaysRun = true)
    public void beforeMethod(Method method) {
        // log start
        System.out
                .println("==================================================================================================================");
        System.out.println("|  Start of test case: " + method.getName());
        System.out
                .println("==================================================================================================================");
    }

    // after method
    @AfterMethod(alwaysRun = true)
    public void afterMethod(Method method, ITestResult tr) {

        // log end
        System.out
                .println("==================================================================================================================");
        System.out.println("|  End of test case: " + method.getName());
        System.out.println("|  Result: " + results[tr.getStatus() - 1]);
        System.out
                .println("==================================================================================================================\n\n");

    }
}
