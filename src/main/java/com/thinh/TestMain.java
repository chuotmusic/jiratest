package com.thinh;

import org.testng.TestNG;

public class TestMain extends TestNG {

    /**
     * @param args
     *            testng.xml
     */
    public static void main(String[] args) {

        if (args != null && args.length > 1) {
            try {
                TestNG.main(args);
            } catch (Exception e) {

            }
        } else {
            System.out.println("LOG [TestMain.main]: running using normal way");
            JiraTest test = new JiraTest();
            System.out.println("=========== running ticket creation test============");
            test.setup();
            test.testTicketCreation();
            test.cleanTest();
            
            System.out.println("=========== running ticket editing test============");
            test.setup();
            test.testTicketCreation();
            test.cleanTest();
            
            System.out.println("=========== running ticket searching test============");
            test.setup();
            test.testSearching();
            test.cleanTest();
        }
        
        
    }

}
