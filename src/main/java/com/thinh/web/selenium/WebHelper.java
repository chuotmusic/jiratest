package com.thinh.web.selenium;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.thinh.utils.ShortcutsTyper;

/**
 * <u><strong>Notes on using Google Chrome:</strong></u>
 * 
 * <p>
 * Chrome Driver is maintained / supported by the <a
 * href="http://code.google.com/p/chromium/">Chromium</a> project itself.
 * WebDriver works with Chrome through the chromedriver binary (found on the
 * chromium project's download page). You need to have both chromedriver and a
 * version of chrome browser installed. chromedriver needs to be placed
 * somewhere on your system's path in order for WebDriver to automatically
 * discover it. The Chrome browser itself is discovered by chromedriver in the
 * default installation path. These both can be overridden by environment
 * variables. Please refer to <a
 * href="http://code.google.com/p/selenium/wiki/ChromeDriver">the wiki</a> for
 * more information.
 * </p>
 * 
 * <p>
 * <i>Last update: Nov 9, 2012</i>
 * </p>
 */
public class WebHelper {
    /*
     * ================================================== Fields
     * ==================================================
     */

    public static final String DEFAULT_BROWSER = "firefox";

    protected WebDriver driver;
    
    private static WebHelper helper = null;

    /*
     * ================================================== Constructors
     * ==================================================
     */

    /**
     * Create a new WebHelper instance with its driver of DEFAULT_BROWSER
     */
    public static WebHelper getHelper(String driverName) {
       if (helper == null) {
           helper = new WebHelper(driverName);
       }
       return helper;
        
    }

    
    public static WebHelper getHelper() {
       return getHelper(DEFAULT_BROWSER);
         
     }

    /**
     * Create a new WebHelper instance with its driver's type taken from sample
     * 
     * @param sample
     *            the sample whose type will be taken by the inside WebDriver
     */
    public WebHelper(WebDriver sample) {
        this(sample.getClass().getSimpleName());
    }
    

    /**
     * Create a new WebHelper instance
     * 
     * @param driverName
     *            browser name, case-insensitive, can be one of these:
     *            <ul>
     *            <li>"firefox", "ff"</li>
     *            <li>"chrome"</li>
     *            <li>"safari" (has not been tested)</li>
     *            <li>"ie", "internetexplorer" (has not been tested)</li>
     *            </ul>
     *            (or "ff")</strong>
     */
    private WebHelper(String driverName) {
        driverName = driverName.toLowerCase();

        if (driverName.equals("firefox") || driverName.equals("firefoxdriver") || driverName.equals("ff")) {
            driver = new FirefoxDriver();
        } else if (driverName.equals("chrome") || driverName.equals("chromedriver")) {
            driver = new ChromeDriver();
        } else if (driverName.equals("safari") || driverName.equals("safaridriver")) {
            driver = new SafariDriver();
        } else if (driverName.equals("ie") || driverName.equals("internetexplorerdriver")
                || driverName.equals("internetexplorer")) {
            driver = new InternetExplorerDriver();
        } else {
            driver = new FirefoxDriver();
        }
    }

    /*
     * ================================================== Gets, sets
     * ==================================================
     */

    /**
     * Get inside WebDriver
     * 
     * @return inside WebDriver
     */
    public WebDriver getDriver() {
        return driver;
    }

    /**
     * Set value for inside WebDriver
     * 
     * @param value
     *            value which the inside WebDriver will take
     */
    public void setDriver(WebDriver value) {
        driver = value;
    }

    /*
     * ================================================== "Finding" methods
     * ==================================================
     */

    /**
     * Find an element, parameter is case-sensitive
     * 
     * @return null if not found
     */
    public WebElement findElementByClassName(String className) {
        try {
            return driver.findElement(By.className(className));
        } catch (Throwable e) {
            System.out.println(e.getClass().getSimpleName() + ": " + e.getMessage());

            return null;
        }
    }

    /**
     * Find a list of elements, parameter is case-sensitive
     * 
     * @return null if not found
     */
    public List<WebElement> findElementsByClassName(String className) {
        try {
            return driver.findElements(By.className(className));
        } catch (Throwable e) {
            System.out.println(e.getClass().getSimpleName() + ": " + e.getMessage());

            return null;
        }
    }

    /**
     * Find an element, parameter is case-sensitive
     * 
     * @return null if not found
     */
    public WebElement findElementByCssSelector(String cssSelector) {
        try {
            return driver.findElement(By.cssSelector(cssSelector));
        } catch (Throwable e) {
            System.out.println(e.getClass().getSimpleName() + ": " + e.getMessage());

            return null;
        }
    }

    /**
     * Find a list of elements, parameter is case-sensitive
     * 
     * @return null if not found
     */
    public List<WebElement> findElementsByCssSelector(String cssSelector) {
        try {
            return driver.findElements(By.cssSelector(cssSelector));
        } catch (Throwable e) {
            System.out.println(e.getClass().getSimpleName() + ": " + e.getMessage());

            return null;
        }
    }

    /**
     * Find an element, parameter is case-sensitive
     * 
     * @return null if not found
     */
    public WebElement findElementById(String id) {
        try {
            return driver.findElement(By.id(id));
        } catch (Throwable e) {
            System.out.println(e.getClass().getSimpleName() + ": " + e.getMessage());

            return null;
        }
    }

    /**
     * Find an element, parameter is case-sensitive
     * 
     * @return null if not found
     */
    public WebElement findElementByLinkText(String linkText) {
        try {
            return driver.findElement(By.linkText(linkText));
        } catch (Throwable e) {
            System.out.println(e.getClass().getSimpleName() + ": " + e.getMessage());

            return null;
        }
    }

    /**
     * Find a list of elements, parameter is case-sensitive
     * 
     * @return null if not found
     */
    public List<WebElement> findElementsByLinkText(String linkText) {
        try {
            return driver.findElements(By.linkText(linkText));
        } catch (Throwable e) {
            System.out.println(e.getClass().getSimpleName() + ": " + e.getMessage());

            return null;
        }
    }

    /**
     * Find an element, parameter is case-sensitive
     * 
     * @return null if not found
     */
    public WebElement findElementByName(String name) {
        try {
            return driver.findElement(By.name(name));
        } catch (Throwable e) {
            System.out.println(e.getClass().getSimpleName() + ": " + e.getMessage());

            return null;
        }
    }

    /**
     * Find an element, parameter is case-sensitive
     * 
     * @return null if not found
     */
    public WebElement findElementByPartialLinkText(String linkText) {
        try {
            return driver.findElement(By.partialLinkText(linkText));
        } catch (Throwable e) {
            System.out.println(e.getClass().getSimpleName() + ": " + e.getMessage());

            return null;
        }
    }

    /**
     * Find a list of elements, parameter is case-sensitive
     * 
     * @return null if not found
     */
    public List<WebElement> findElementsByPartialLinkText(String linkText) {
        try {
            return driver.findElements(By.partialLinkText(linkText));
        } catch (Throwable e) {
            System.out.println(e.getClass().getSimpleName() + ": " + e.getMessage());

            return null;
        }
    }

    /**
     * Find an element, parameter is case-insensitive
     * 
     * @return null if not found
     */
    public WebElement findElementByTagName(String name) {
        try {
            return driver.findElement(By.tagName(name));
        } catch (Throwable e) {
            System.out.println(e.getClass().getSimpleName() + ": " + e.getMessage());

            return null;
        }
    }

    /**
     * Find a list of elements, parameter is case-insensitive
     * 
     * @return null if not found
     */
    public List<WebElement> findElementsByTagName(String name) {
        try {
            return driver.findElements(By.tagName(name));
        } catch (Throwable e) {
            System.out.println(e.getClass().getSimpleName() + ": " + e.getMessage());

            return null;
        }
    }

    /**
     * Find an element, parameter is case-sensitive
     * 
     * @return null if not found
     */
    public WebElement findElementByXpath(String xpathExpression) {
        try {
            return driver.findElement(By.xpath(xpathExpression));
        } catch (Throwable e) {
            System.out.println(e.getClass().getSimpleName() + ": " + e.getMessage());

            return null;
        }
    }

    /**
     * Find a list of elements, parameter is case-sensitive
     * 
     * @return null if not found
     */
    public List<WebElement> findElementsByXpath(String xpathExpression) {
        try {
            return driver.findElements(By.xpath(xpathExpression));
        } catch (Throwable e) {
            System.out.println(e.getClass().getSimpleName() + ": " + e.getMessage());

            return null;
        }
    }

    /**
     * Check if an element exists
     * 
     * @param identifier
     *            identifier of element
     * @return boolean
     */
    public boolean elementExists(By identifier) {
        try {
            driver.findElement(identifier);

            return true;
        } catch (Throwable e) {
            return false;
        }
    }

    /**
     * Check if an element exists with time out
     * 
     * @param identifier
     *            identifier of element
     * @param timeOutInSeconds
     *            time out in seconds
     * @return boolean
     */
    public boolean elementExists(final By identifier, long timeOutInSeconds) {
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);

        ExpectedCondition<WebElement> elementAppears = new ExpectedCondition<WebElement>() {
            public WebElement apply(WebDriver d) {
                try {
                    return d.findElement(identifier);
                } catch (Throwable e) {
                    return null;
                }
            }
        };

        try {
            wait.until(elementAppears);

            return true;
        } catch (Throwable e) {
            System.out.println(e.getClass().getSimpleName() + ": " + e.getMessage());

            return false;
        }
    }

    /**
     * Check if a text is contained in page source
     */
    public boolean textExists(String text) {
        return textExists(text, 100);
    }
    
    /**
     * Check if a text is showing, which a timeout period in milliseconds
     * @param text
     * @param timeout time in milliseconds
     * @return
     */
    public boolean textExists(String text, long timeout) {
        if (driver == null) {
            return false;
        }
        long start = System.currentTimeMillis();
        long end = System.currentTimeMillis();
        boolean found = false;
        while (end - start < timeout) {
            found = driver.getPageSource().contains(text);
            end = System.currentTimeMillis();
            if (found) {
                return true;
            } else {
                ShortcutsTyper.delayTime(200);
            }
        }
        return found;
    }

    /*
     * ================================================== "Waiting" methods
     * ==================================================
     */

    /**
     * Wait for an element to appear with default time out (5 seconds)
     * 
     * @param by
     *            condition to find the element
     * @return null or WebElement
     */
    public WebElement waitForElement(By by) {
        return waitForElement(by, 5);
    }

    /**
     * Wait for an element to appear with time out
     * 
     * @param by
     *            condition to find the element
     * @param timeOutInSeconds
     *            time out in seconds
     * @return null or WebElement
     */
    public WebElement waitForElement(final By by, long timeOutInSeconds) {
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);

        ExpectedCondition<WebElement> elementAppears = new ExpectedCondition<WebElement>() {
            public WebElement apply(WebDriver d) {
                try {
                    return d.findElement(by);
                } catch (Throwable e) {
                    return null;
                }
            }
        };

        try {
            return wait.until(elementAppears);
        } catch (Throwable e) {
            System.out.println(e.getClass().getSimpleName() + ": " + e.getMessage());

            return null;
        }
    }

    /**
     * Delay 1 second
     */
    public void delay() {
        delay(1);
    }

    /**
     * Delay with provided time out in seconds
     */
    public void delay(long timeOutInSeconds) {
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        ExpectedCondition<Boolean> dilemma = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return false;
            }
        };

        try {
            wait.until(dilemma);
        } catch (Throwable e) {
        }
    }

    /*
     * ================================================== Navigation
     * ==================================================
     */

    /**
     * Go to provided URL
     */
    public void goTo(String ulr) {
        driver.get(ulr);
    }

    /**
     * Navigate back to previous web page
     */
    public void goBack() {
        if (driver.getClass().getSimpleName().equals("SafariDriver")) {
            System.out.println("Navigating through the browser history does " + "not work for the SafariDriver");

            return;
        }

        driver.navigate().back();
    }

    /**
     * Navigate forward to next web page
     */
    public void goForward() {
        if (driver.getClass().getSimpleName().equals("SafariDriver")) {
            System.out.println("Navigating through the browser history does " + "not work for the SafariDriver");

            return;
        }

        driver.navigate().forward();
    }

    /**
     * Refresh web page
     */
    public void refreshPage() {
        driver.navigate().refresh();
    }

    /*
     * ================================================== Others
     * ==================================================
     */

    /**
     * Print out to console
     */
    public void print(String text) {
        System.out.print(text);
    }
    
    public String getURL(String text) {
        WebElement link = driver.findElement(By.partialLinkText(text));
        String linkLocatin = link.getAttribute("href");
        System.out.println("Link Location "+linkLocatin);
        return linkLocatin;
    }
    
    public void quit() {
        driver.quit();
        driver = null;
    }
}
