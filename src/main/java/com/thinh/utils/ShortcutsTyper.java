package com.thinh.utils;

import java.awt.AWTException;
import java.awt.MouseInfo;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Hashtable;
import java.util.Map;

/**
 * Used to type Shortcuts. Contains many kinds of common used Shortcuts. Support
 * windows system and Mac system. Mac unicode characters only support Swedish
 * Keyboard
 * 
 */
public class ShortcutsTyper {
    protected static Robot robot;
    private static final int defaultDelay = 150;
    /*
     * Windows: false. Mac: true;
     */
    protected static boolean mac = false;
    static {
        try {
            robot = new Robot();
//            mac = OSValidator.isMac();
        } catch (AWTException e) {
            e.printStackTrace();
        }
        robot.setAutoDelay(defaultDelay);
        robot.setAutoWaitForIdle(true);
    }
    @SuppressWarnings("serial")
    private static final Map<Integer, Integer> keyMap_shift_mac = new Hashtable<Integer, Integer>() {
        {
            // /7
            put(47, 55);
            // =0
            put(61, 48);
            // ?+
            put(63, 45);
            // :.
            put(58, 46);
            // _-
            put(95, 47);
            // ;,
            put(59, 44);
            // *'
            put(42, 92);
        }
    };
    @SuppressWarnings("serial")
    private static final Map<Integer, Integer> keyMap_alt_shift_mac = new Hashtable<Integer, Integer>() {
        {
            put(92, 55);
            put(123, 56);
            put(125, 57);
        }
    };
    @SuppressWarnings("serial")
    private static final Map<Integer, Integer> keyMap_alt_mac = new Hashtable<Integer, Integer>() {
        {
            put(36, 52);
            put(91, 56);
            put(93, 57);
        }
    };

    public static Robot getRobot() {
        return robot;
    }

    /**
     * Make delay time.
     * 
     * @param millis
     *            the length of time to sleep in milliseconds.
     */
    public static void delayTime(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    /**
     * Make delay time.
     * 
     * @param millis
     *            the length of time to sleep in milliseconds.
     */
    public static void delayOne() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Set the action delay time
     * 
     * @param millis
     */
    public static void setActionDelayTime(int millis) {
        robot.setAutoDelay(millis);
    }

    /**
     * Set the action delay time to default.
     * 
     * @param millis
     */
    public static void setActionDelayTimeToDefault() {
        robot.setAutoDelay(defaultDelay);
    }

    /**
     * Press and Release a key.
     * 
     * @param key
     */
    public static void typeKey(int key) {
        robot.keyPress(key);
        robot.keyRelease(key);
    }

    /**
     * Press key with Ctrl mask
     * 
     * @param key
     */
    public static void ctrlMaskPress(int key) {
        robot.keyPress(KeyEvent.VK_CONTROL);
        typeKey(key);
        robot.keyRelease(KeyEvent.VK_CONTROL);

    }

    /**
     * Press key with Alt mask
     * 
     * @param key
     */
    public static void AltMaskPress(int key) {
        robot.keyPress(KeyEvent.VK_ALT);
        typeKey(key);
        robot.keyRelease(KeyEvent.VK_ALT);
    }

    /**
     * Press key with Alt+shift mask
     * 
     * @param key
     */
    public static void AltShiftMaskPress(int key) {
        robot.keyPress(KeyEvent.VK_ALT);
        robot.keyPress(KeyEvent.VK_SHIFT);
        typeKey(key);
        robot.keyRelease(KeyEvent.VK_SHIFT);
        robot.keyRelease(KeyEvent.VK_ALT);
    }

    /**
     * Press key with shift mask
     * 
     * @param key
     */
    public static void shiftMaskPress(int key) {
        robot.keyPress(KeyEvent.VK_SHIFT);
        typeKey(key);
        robot.keyRelease(KeyEvent.VK_SHIFT);
    }

    /**
     * Press key with META mask. For Mac system.
     * 
     * @param key
     */
    public static void cmdMaskPress(int key) {
        robot.keyPress(KeyEvent.VK_META);
        typeKey(key);
        robot.keyRelease(KeyEvent.VK_META);

    }

    /**
     * Press key with Ctrl-Shift mask
     * 
     * @param key
     */
    public static void ctrlShiftMaskPress(int key) {
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_SHIFT);
        typeKey(key);
        robot.keyRelease(KeyEvent.VK_SHIFT);
        robot.keyRelease(KeyEvent.VK_CONTROL);
    }

    /**
     * Press key with META-Shift mask. For Mac system.
     * 
     * @param key
     */
    public static void cmdShiftMaskPress(int key) {
        robot.keyPress(KeyEvent.VK_META);
        robot.keyPress(KeyEvent.VK_SHIFT);
        typeKey(key);
        robot.keyRelease(KeyEvent.VK_SHIFT);
        robot.keyRelease(KeyEvent.VK_META);
    }

    /**
     * Press key with Ctrl-alt mask
     * 
     * @param key
     */
    public static void ctrlAltMaskPress(int key) {
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_ALT);
        typeKey(key);
        robot.keyRelease(KeyEvent.VK_ALT);
        robot.keyRelease(KeyEvent.VK_CONTROL);
    }

    /**
     * Press key with META-Shift mask. For Mac system.
     * 
     * @param key
     */
    public static void cmdAltMaskPress(int key) {
        robot.keyPress(KeyEvent.VK_META);
        robot.keyPress(KeyEvent.VK_ALT);
        typeKey(key);
        robot.keyRelease(KeyEvent.VK_ALT);
        robot.keyRelease(KeyEvent.VK_META);
    }

    /**
     * Press key with META-Ctrl mask. For Mac system.
     * 
     * @param key
     */
    public static void cmdCtrlMaskPress(int key) {
        robot.keyPress(KeyEvent.VK_META);
        robot.keyPress(KeyEvent.VK_CONTROL);
        typeKey(key);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyRelease(KeyEvent.VK_META);
    }

    /**
     * Enable debug mode
     */
    public static void enableDebugMode() {
        if (!mac) {
            ctrlAltMaskPress(KeyEvent.VK_HOME);
        } else {
            cmdAltMaskPress(KeyEvent.VK_HOME);
        }
    }


    /**
     * Cut Ctrl-X Cmd-X
     */
    public static void cut() {
        if (!mac) {
            ctrlMaskPress(KeyEvent.VK_X);
        } else {
            cmdMaskPress(KeyEvent.VK_X);
        }
    }

  
   
    /**
     * Delete Del Del, Backspace
     */
    public static void delete() {
        typeKey(KeyEvent.VK_DELETE);
    }

    /**
     * Select all Ctrl-A Cmd-A
     */
    public static void selectAll() {
        if (!mac) {
            ctrlMaskPress(KeyEvent.VK_A);
        } else {
            cmdMaskPress(KeyEvent.VK_A);
        }
    }

    /**
     * Select none Ctrl-Shift-A Cmd-Shift-A
     */
    public static void selectNone() {
        if (!mac) {
            ctrlShiftMaskPress(KeyEvent.VK_A);
        } else {
            cmdShiftMaskPress(KeyEvent.VK_A);
        }
    }

   
    /**
     * Preferences Ctrl-P Cmd-,
     */


    /**
     * only mac system Hide window - Cmd-H
     */
    public static void hideWindow() {
        if (mac) {
            cmdMaskPress(KeyEvent.VK_H);
        }
    }

    /**
     * only mac system Hide other applications' windows - Cmd-Alt-H
     */
    public static void hideOtherApplicationsWindows() {
        if (mac) {
            robot.keyPress(KeyEvent.VK_META);
            robot.keyPress(KeyEvent.VK_ALT);
            typeKey(KeyEvent.VK_H);
            robot.keyRelease(KeyEvent.VK_ALT);
            robot.keyRelease(KeyEvent.VK_META);
        }
    }

    /**
     * only mac system Close window - Cmd-W
     */
    public static void closeWindow() {
        if (mac) {
            cmdMaskPress(KeyEvent.VK_W);
        }
    }

    /**
     * only mac system Minimize window - Cmd-M
     */
    public static void minimizeWindow() {
        if (mac) {
            cmdMaskPress(KeyEvent.VK_M);
        }
    }

    /**
     * only mac system Restore window (from hidden state) - Cmd-Alt-1
     */
    public static void restoreWindow() {
        if (mac) {
            robot.keyPress(KeyEvent.VK_META);
            robot.keyPress(KeyEvent.VK_ALT);
            typeKey(KeyEvent.VK_1);
            robot.keyRelease(KeyEvent.VK_ALT);
            robot.keyRelease(KeyEvent.VK_META);
        }
    }

   
    /**
     * keycode handles [A-Z] [a-z] [0-9]!\"#%&/()=?*_:;@${[]} \\+',.-
     * 
     * @param s
     */
    public static void type(String s) {
        byte[] bytes = s.getBytes();
        for (byte b : bytes) {
            int code = b;
            if (code > 96 && code < 123) {
                code = code - 32;
                typeKey(code);
            } else if (code > 47 && code < 58) {
                typeKey(code);
            } else if (code > 64 && code < 91) {
                shiftMaskPress(code);
            } else {
                typeOtherKeys(code);
            }
        }
    }

    public static void typeOtherKeys(int key) {
        setActionDelayTime(50);
        if (!mac) {
            robot.keyPress(KeyEvent.VK_ALT);
            for (int i = 3; i >= 0; --i) {
                int num = key / (int) (Math.pow(10, i)) % 10 + KeyEvent.VK_NUMPAD0;
                typeKey(num);
            }
            robot.keyRelease(KeyEvent.VK_ALT);
        } else {
            if (keyMap_shift_mac.containsKey(key)) {
                shiftMaskPress(keyMap_shift_mac.get(key));
            } else if (keyMap_alt_shift_mac.containsKey(key)) {
                AltShiftMaskPress(keyMap_alt_shift_mac.get(key));
            } else if (keyMap_alt_mac.containsKey(key)) {
                AltMaskPress(keyMap_alt_mac.get(key));
            } else if ((36 > key && key > 32) || key == 37 || key == 38 || key == 40 || key == 41) {
                shiftMaskPress(key + 16);
            } else if (key == 43) {
                typeKey(45);
            } else if (key == 39) {
                typeKey(92);
            } else if (key == 45) {
                typeKey(47);
            } else if (key == 64) {
                AltMaskPress(50);
            } else {
                typeKey(key);
            }
        }
        setActionDelayTimeToDefault();
    }

    /**
     * Rotates the scroll wheel on wheel-equipped mice.
     * 
     * @param wheelAmt
     *            number of "notches" to move the mouse wheel Negative values
     *            indicate movement up/away from the user, positive values
     *            indicate movement down/towards the user.
     */
    public static void mouseWheel(int wheelAmt) {
        robot.mouseWheel(wheelAmt);
    }

    /**
     * Moves mouse pointer to given screen coordinates
     * 
     * @param x
     * @param y
     */
    public static void moveMouse(int x, int y) {
        robot.mouseMove(x, y);
    }

    public static void mouseClick(int key) {
        robot.mousePress(key);
        robot.mouseRelease(key);
    }

    /**
     * Move the mouse and click.
     * 
     * @param x
     * @param y
     * @param key
     */
    public static void mouseClick(int x, int y, int key) {
        moveMouse(x, y);
        mouseClick(key);
    }

    public static void mouseDoubleClick(int key) {
        mouseClick(key);
        mouseClick(key);
    }

    public static void mouseDoubleClick(int x, int y, int key) {
        moveMouse(x, y);
        mouseDoubleClick(key);
    }

    /**
     * Get the coordinates of mouse pointer
     * 
     * @return En int array with [0]-x,[1]-y
     */
    public static int[] getMouseLocation() {
        int[] location = { MouseInfo.getPointerInfo().getLocation().x, MouseInfo.getPointerInfo().getLocation().y };
        return location;
    }

    /**
     * Press enter key.
     */
    public static void enter() {
        typeKey(KeyEvent.VK_ENTER);
    }
}
