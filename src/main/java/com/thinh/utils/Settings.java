package com.thinh.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;

public class Settings {

    private static HashMap<String, String> map = new HashMap<String, String>();

    static {
        String propFile = "test.properties";
        Properties properties = new Properties();
        // load the properties from default place first
        try {
            InputStream is = ClassLoader.getSystemResourceAsStream(propFile);
            if (is == null) {
                throw new FileNotFoundException("Could not find file: " + propFile);
            }
            properties.load(is);
        } catch (Exception e) {
            System.out.println("LOG [Settings.enclosing_method]: " + e);
            System.exit(1);
        }

        // load overridden properties
        File file = new File(propFile);
        if (file.exists()) {
            Properties override = new Properties();
            try {
                override.load(new FileInputStream(file));
                for (Object key : override.keySet()) {
                    properties.put(key, override.get(key));
                }
            } catch (Exception e) {
                // just ignore if exception
            }
        }

        Set<String> keys = properties.stringPropertyNames();
        for (String key : keys) {
            map.put(key, properties.getProperty(key));
        }
    }

    public static String getValue(String key) {
        String result = map.get(key);
        return result;
    }

    public static void put(String key, String value) {
        map.put(key, value);
    }

    /**
     * Load a certain parameter from system property or test.properties file.
     * 
     * @param param
     * @return
     */
    public static String getParameter(String param) {
        String current = "";
        current = System.getProperty(param);
        if (current == null || current.length() < 1) {
            current = Settings.getValue(param);
        }
        return current;
    }
    
    public static int getInt(String key) {
        String value = getParameter(key);
        int result =0;
        if (value != null) {
            result = Integer.parseInt(value);
        }
        
        return result;
    }
    
    public static double getDouble(String key) {
        String value = getParameter(key);
        double result =0;
        if (value != null) {
            result = Double.parseDouble(value);
        }
        
        return result;
    }
}
