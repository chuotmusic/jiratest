package com.thinh;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.thinh.utils.ShortcutsTyper;
import com.thinh.views.Home;
import com.thinh.views.Login;
import com.thinh.views.SearchBox;
import com.thinh.views.TicketCreator;
import com.thinh.views.TicketEditor;
import com.thinh.views.TicketView;
import com.thinh.views.data.Ticket;
import com.thinh.web.selenium.WebHelper;

public class JiraTest extends AbstractTest {

    /**
     * This test does these things:
     * - open jira demo project
     * - log in if needed
     * - create a ticket with minimal required info
     * - verify the ticket is created
     */
    @Test(groups = { "jira", "create"})
    public void testTicketCreation() {
        Login.login();
        Home.openTicketCreator();
        Ticket ticket = new Ticket();
        ticket.setSummary("Sample ticket " + System.currentTimeMillis());
        ticket.setDescription("This is my awesome ticket");
        String ticketURL = TicketCreator.createATicket(ticket, false);
        System.out.println("LOG [Debug.main]: url = " + ticketURL);
        Assert.assertTrue(ticketURL != null, "The ticket URL was null");
    }
    
    /**
     * This test does these things:
     * - open jira demo project
     * - log in if needed
     * - open a predefined ticket
     * - change the summary
     * - verify that the summary has been changed
     */
    @Test(groups = { "jira", "edit"})
    public void testTicketEditing() {
        Login.login();
        
        WebHelper.getHelper().goTo(ticket);
        ShortcutsTyper.delayTime(5000);
        TicketView.editTicket();
        Ticket updatedTicket = new Ticket();
        String summary = "Updated at " + System.currentTimeMillis();
        updatedTicket.setSummary(summary);
        TicketEditor.editTicket(updatedTicket);
        
        ShortcutsTyper.delayOne();
        String newSummary = TicketView.getTicketSummary();
        Assert.assertTrue(summary.equalsIgnoreCase(newSummary), "Failed: old= "+ summary + ", but new=" + newSummary);
    }
    
    /**
     * This test does these things:
     * - open jira demo project
     * - search for a ticket using its summary
     * - verify that the result is showing up in the first one
     */
    @Test(groups = { "jira", "search"})
    public void testSearching() {
        WebHelper.getHelper().goTo(ticket);
        ShortcutsTyper.delayTime(5000);
        String summary = TicketView.getTicketSummary();
        SearchBox.search(summary);
        ShortcutsTyper.delayTime(3000);
        
        String result = SearchBox.getFirstSearchResultSummary();
        Assert.assertTrue(summary.equalsIgnoreCase(result), "Expected: " + summary + ", actual: "+ result);
        
    }

}
