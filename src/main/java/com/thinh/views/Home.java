package com.thinh.views;

import com.thinh.utils.ShortcutsTyper;
import com.thinh.web.selenium.WebHelper;

public class Home {

    public static void openTicketCreator() {
        WebHelper.getHelper().findElementById("create_link").click();
        ShortcutsTyper.delayTime(5000);
    }
}
