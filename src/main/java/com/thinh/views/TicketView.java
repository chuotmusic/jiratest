package com.thinh.views;

import com.thinh.utils.ShortcutsTyper;
import com.thinh.web.selenium.WebHelper;

public class TicketView {

  public static void editTicket() {
      WebHelper.getHelper().findElementById("edit-issue").click();
      ShortcutsTyper.delayTime(2000);
  }
  
  public static String getTicketSummary() {
      return WebHelper.getHelper().findElementById("summary-val").getText();
  }

}
