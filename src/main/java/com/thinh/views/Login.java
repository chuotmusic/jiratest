package com.thinh.views;

import org.openqa.selenium.By;

import com.thinh.web.selenium.WebHelper;

public class Login {

    public static void logout() {

    }

    /**
     * Assume we are at the main page
     */
    public static void login() {
        if (!WebHelper.getHelper().textExists("Log In", 5)) {
            System.out.println("LOG [Login.login]: already logged in");
            return;
        }
        WebHelper.getHelper().waitForElement(By.linkText("Log In"), 5);
        WebHelper.getHelper().findElementByLinkText("Log In").click();
        WebHelper.getHelper().waitForElement(By.linkText("Unable to access your account?"), 10);
        WebHelper.getHelper().findElementByName("username").sendKeys("thinh.le.vn@gmail.com");
        WebHelper.getHelper().findElementByName("password").sendKeys("Thinh123");
        WebHelper.getHelper().findElementById("login-submit").click();
        Login.closeWhatsNew();
    }

    public static void closeWhatsNew() {
        WebHelper.getHelper().waitForElement(By.linkText("Close"), 10);
        try {
            WebHelper.getHelper().findElementByLinkText("Close").click();
        } catch (Exception e) {

        }
    }

}
