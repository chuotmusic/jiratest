package com.thinh.views;

import com.thinh.utils.ShortcutsTyper;
import com.thinh.views.data.Ticket;
import com.thinh.web.selenium.WebHelper;

public class TicketCreator {
    
    public static String createATicket(Ticket ticket, boolean createMore)  {
     
        String url;
        if (ticket.getSummary() != null) {
            WebHelper.getHelper().findElementById("summary").sendKeys(ticket.getSummary());
        }
        if (ticket.getDescription() != null) {
            WebHelper.getHelper().findElementById("description").sendKeys(ticket.getDescription());
        }
        
        WebHelper.getHelper().findElementById("create-issue-submit").click();
        
        
        //how to wait dynamically here
//        ShortcutsTyper.delayTime(6000);
        if (WebHelper.getHelper().textExists("has been successfully created", (long)15000)) {
            url = WebHelper.getHelper().getURL(ticket.getSummary());
            return url;
        }
        return null;
    }
    
}


