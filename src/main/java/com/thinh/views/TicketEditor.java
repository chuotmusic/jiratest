package com.thinh.views;

import com.thinh.views.data.Ticket;
import com.thinh.web.selenium.WebHelper;

public class TicketEditor {

    public static String editTicket(Ticket updatedTicket) {
        String url;
        if (updatedTicket.getSummary() != null) {
            WebHelper.getHelper().findElementById("summary").sendKeys(updatedTicket.getSummary());
        }
        if (updatedTicket.getDescription() != null) {
            WebHelper.getHelper().findElementById("description").sendKeys(updatedTicket.getDescription());
        }
        
        WebHelper.getHelper().findElementById("edit-issue-submit").click();
        
        
        //how to wait dynamically here
//        ShortcutsTyper.delayTime(4000);
        if (WebHelper.getHelper().textExists("has been successfully created", 15000)) {
            url = WebHelper.getHelper().getURL(updatedTicket.getSummary());
            return url;
        }
        return null;
    }
}
