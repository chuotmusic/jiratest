package com.thinh.views;

import java.awt.event.KeyEvent;

import com.thinh.utils.ShortcutsTyper;
import com.thinh.web.selenium.WebHelper;

public class SearchBox {

    public static void search(String text) {
        WebHelper.getHelper().findElementById("quickSearchInput").sendKeys(text);
        ShortcutsTyper.typeKey(KeyEvent.VK_ENTER);
        System.out.println("LOG [SearchBox.search]: searching with keyword: " + text);
        
        WebHelper.getHelper().textExists("Save as", 15000);
    }
    
    public static String getFirstSearchResultSummary() {
        if (WebHelper.getHelper().findElementById("search-header-view") == null) {
            return null;
        } else {
            return WebHelper.getHelper().findElementById("summary-val").getText();
        }
        
    }

}
